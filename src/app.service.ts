import { Test } from './schemas/test.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';



@Injectable()
export class AltService {

constructor(@InjectModel('Test') private testModel:Model<Test>){}

  async getHelloAG(): Promise<any> {
    const res =  this.testModel.find();
    return await res;
  }
}

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}