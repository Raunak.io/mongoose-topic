import { AltModule } from './alt.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController, AltController } from './app.controller';
import { AppService, AltService } from './app.service';

@Module({
  imports: [
    AltModule,

    MongooseModule.forRoot('mongodb+srv://healthMaddy:oqCB7zjvMLtmwzdo@cluster0-fjdb2.mongodb.net/basic-test?retryWrites=true&w=majority'),// we can also use async version
  ],
  // just for example  // MongooseModule.forRoot('mongodb://localhost:27017/testing',{
  // connection:'' //route name
  // }),  //after this set up feature module  
  // MongooseModule.forRoot('mongodb://localhost:27017/testing',{
    // connection:'newOne' //route2 name
    // }),
// ],
  controllers: [AppController, AltController],
  providers: [AppService],
})
export class AppModule {}
