import  {TestsSchema } from './schemas/test.schema';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AltController } from './app.controller';
import { AltService } from './app.service';

@Module({
    imports: [MongooseModule.forFeature([{name:'Test',schema:TestsSchema}])],
  // imports: [
  //   MongooseModule.forFeatureAsync([ // a pre hook   in async version
  //     {
  //       name: 'Tests',
  //       useFactory: () => {
  //         const schema = TestSchema;
  //         schema.pre('save', () =>
  //           console.log('pre hook middleware by mongoose'),
  //         );
  //       },
  //     },
  //   ]),
  // ],
  //   imports: [MongooseModule.forFeature([{name:'Test',schema:'TestSchema'}],'')], //route name
  controllers: [AltController],
  providers: [AltService],
  exports:[AltService]
})
export class AltModule {}
