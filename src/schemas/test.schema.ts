import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

// @Schema()
// export class Tests  extends mongoose.Document{
//   // later test will be saved as tests in database collection

//   @Prop() // stands for property

//   // @Prop({required:true})
//   name: string;
// }
// we can define schema manually also
export const TestsSchema = new mongoose.Schema({
  name: String
});

// export const TestSchema = SchemaFactory.createForClass(Tests);

export interface Test extends mongoose.Document{
    name:string
}