import { Controller, Get } from '@nestjs/common';
import { AppService, AltService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}

@Controller('newOne')
export class AltController {
  constructor(private readonly appService: AltService) {}

  @Get()
 async getHelloAGAIN(): Promise<any> {
    return ;
  }
}
